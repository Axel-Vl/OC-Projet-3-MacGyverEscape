from random import randint
from content.classes.tile import Tile


class Items:
    """Generate items and display them in the labyrinth"""

    def __init__(self, screen, tiles_info_list):
        self.screen = screen
        self.tiles_info_list = tiles_info_list
        self.dict = {}
        self._generate_tiles()

    def _generate_tiles(self):
        """Create tiles and add them to the tiles dict"""
        ether_created = 0
        needle_created = 0
        plastic_tube_created = 0
        guardian_created = 0
        unvailable_pos = []

        while guardian_created == 0:
            x = 19
            y = 19
            tile_type = 'G'
            key = 'G'
            tile = Tile(x, y, tile_type)
            self.dict[key] = tile
            guardian_created += 1
            unvailable_pos.append((x, y))

        while ether_created < 4:
            z = randint(0, len(self.tiles_info_list) - 1)
            if self.tiles_info_list[z][2] == ' ':
                x = self.tiles_info_list[z][0]
                y = self.tiles_info_list[z][1]
                if (x, y) in unvailable_pos:
                    pass
                elif (x, y) not in unvailable_pos:
                    tile_type = 'ETHER'
                    key = 'ETHER' + str(ether_created + 1)
                    tile = Tile(x, y, tile_type)
                    self.dict[key] = tile
                    ether_created += 1
                    unvailable_pos.append((x, y))

        while needle_created == 0:
            z = randint(0, len(self.tiles_info_list) - 1)
            if self.tiles_info_list[z][2] == ' ':
                x = self.tiles_info_list[z][0]
                y = self.tiles_info_list[z][1]
                if (x, y) in unvailable_pos:
                    pass
                elif (x, y) not in unvailable_pos:
                    tile_type = 'NEEDLE'
                    key = 'NEEDLE'
                    tile = Tile(x, y, tile_type)
                    self.dict[key] = tile
                    needle_created += 1
                    unvailable_pos.append((x, y))

        while plastic_tube_created == 0:
            z = randint(0, len(self.tiles_info_list) - 1)
            if self.tiles_info_list[z][2] == ' ':
                x = self.tiles_info_list[z][0]
                y = self.tiles_info_list[z][1]
                if (x, y) in unvailable_pos:
                    pass
                elif (x, y) not in unvailable_pos:
                    tile_type = 'PLASTIC_TUBE'
                    key = 'PLASTIC_TUBE'
                    tile = Tile(x, y, tile_type)
                    self.dict[key] = tile
                    plastic_tube_created += 1
                    unvailable_pos.append((x, y))

    def blitme(self):
        """Blit all the items in the labyrinth"""
        for tile in self.dict.values():
            if tile == None:
                pass
            else:
                self.screen.blit(tile.image, tile.rect)
