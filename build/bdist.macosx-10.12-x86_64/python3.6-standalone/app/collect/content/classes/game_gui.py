import pygame
from pygame.locals import *
from sys import exit
from content import general_functions as gf
from content.classes.hero import Hero
from content.classes.labyrinth import Labyrinth
from content.classes.items import Items


class Gui:
    """
    The Graphical User Interface for our game,
    manage all the users input and generate the main objects needed for the
    game,
    also manage the screen refresh for the game

    """

    def __init__(self):
        pygame.init()
        pygame.display.set_caption("Mc Gyver Escape")
        self.clock = pygame.time.Clock()
        self.screen = pygame.display.set_mode((840, 840), 0, 16)
        self.run = 0

        # Global images / rect
        self.background_image = gf.load_img("content/images/background.png")
        self.winning_image = gf.load_img("content/images/win.png")
        self.losing_image = gf.load_img("content/images/lose.png")

        # Initialize objects
        self.labyrinth = Labyrinth(self.screen)
        self.items = Items(self.screen,
                           self.labyrinth.tiles_info_list)
        self.hero = Hero(self.screen,
                         self.labyrinth.walls_tiles_list,
                         self.items)

    def mainloop(self):
        """Mainloop of the game, caps the screen refresh to 60hz"""
        while True:
            if self.run == 0:
                self.check_events()
                self.hero.update_pos()
                self.hero.update_inventory()
                self.hero.update_state()
                self.game_state()
                self.update_screen()
            elif self.run == 1 or 2:
                self.check_events()
                self.update_screen()
            self.clock.tick(60)

    def update_screen(self):
        """
        Blit every object of the game (hero, walls, items) and then draw
        them on the screen,
        also manage the win/loose image display

        """
        self.screen.blit(self.background_image, (0, 0))
        self.labyrinth.blitme()
        self.hero.blitme()
        self.hero.inventory.blit_counters()
        self.items.blitme()
        if self.run == 1:
            self.game_state_blit(self.winning_image)
        elif self.run == 2:
            self.game_state_blit(self.losing_image)
        pygame.display.flip()

    def check_events(self):
        """Wait for user inputs and manage them"""
        for event in pygame.event.get():
            self.quit_game(event)
            self.keydown_events(event)
            self.keyup_events(event)

    def keydown_events(self, event):
        """Manage all the events when a key is pressed"""
        if event.type == KEYDOWN:
            if event.key == K_UP:
                self.hero.moving_up = True
            elif event.key == K_DOWN:
                self.hero.moving_down = True
            elif event.key == K_LEFT:
                self.hero.moving_left = True
            elif event.key == K_RIGHT:
                self.hero.moving_right = True
            elif event.key == K_SPACE:
                self.hero.speed_factor *= 2
            elif event.key == K_ESCAPE:
                pygame.quit()
                exit()

    def keyup_events(self, event):
        """Manage all the events when a key is released"""
        if event.type == KEYUP:
            if event.key == K_UP:
                self.hero.moving_up = False
                self.hero.y_speed = 0
            elif event.key == K_DOWN:
                self.hero.moving_down = False
                self.hero.y_speed = 0
            elif event.key == K_LEFT:
                self.hero.moving_left = False
                self.hero.x_speed = 0
            elif event.key == K_RIGHT:
                self.hero.moving_right = False
                self.hero.x_speed = 0
            elif event.key == K_SPACE:
                self.hero.speed_factor /= 2

    def quit_game(self, event):
        """Allow the user to quit the game with ESC or the red cross"""
        if event.type == QUIT:
            pygame.quit()
            exit()

    def game_state(self):
        """Manage the game state, win or loose"""
        if self.hero.fight_guardian() is True:
            self.run = 1
        elif self.hero.fight_guardian() is False:
            self.run = 2

    def game_state_blit(self, image):
        """Display the win or loose image"""
        self.screen.blit(image, (0, 0))
