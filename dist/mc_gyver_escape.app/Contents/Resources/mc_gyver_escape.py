#! /usr/bin/env python3


from content.classes.game_gui import Gui


def main():
    gui = Gui()
    gui.mainloop()


if __name__ == '__main__':
    main()
