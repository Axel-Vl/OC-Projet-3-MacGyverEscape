import pygame
from pygame.font import SysFont


class Inventory:
    """A class made to manage the Mc Gyver inventory"""

    def __init__(self, items_dict, screen):
        self.screen = screen
        self.plastic_tube = 0
        self.plastic_tube_counter = SysFont("comicsansms", 30)
        self.needle = 0
        self.needle_counter = SysFont("comicsansms", 30)
        self.ether = 0
        self.ether_counter = SysFont("comicsansms", 30)
        self.items_dict = items_dict

    def add_plastic_tube(self, key):
        self.plastic_tube += 1

    def add_needle(self, key):
        self.needle += 1

    def add_ether(self, key):
        self.ether += 1

    def is_full(self):
        """
        Check if the Mc Gyver inventory meet the requirements to defeat
        the guardian

        """
        if self.plastic_tube == 1 and self.needle == 1 and self.ether == 4:
            return True
        elif self.plastic_tube != 1 or self.needle != 1 or self.ether != 4:
            return False

    def blit_counters(self):
        """Display on the screen all the items counters"""
        ptc_text = self.plastic_tube_counter.render("Plastic Tube: " +
                                                    str(self.plastic_tube),
                                                    True,
                                                    (255, 255, 255))
        n_text = self.needle_counter.render("Needle: " +
                                            str(self.needle),
                                            True,
                                            (255, 255, 255))
        e_text = self.ether_counter.render("Ether: " +
                                           str(self.ether),
                                           True,
                                           (255, 255, 255))
        self.screen.blit(ptc_text, (10, 800))
        self.screen.blit(n_text, (210, 800))
        self.screen.blit(e_text, (410, 800))
