from pygame.sprite import Sprite
from content import general_functions as gf
from content.classes.inventory import Inventory


class Hero(Sprite):
    """
    Manage all the functions related to the Hero of our game (Mc Gyver here)
    Allow the hero to move around the labyrinth and to pick items

    """

    def __init__(self, screen, walls_list, items):
        Sprite.__init__(self)
        self.image, self.rect = gf.load_img_rect('content/images/hero.png')
        self.rect = self.rect.move(132, 132)  # Init position in the labyrinth
        self.screen = screen
        self.walls_list = walls_list
        self.items = items
        self.can_escape = False

        # Create Mc Gyver inventory
        self.inventory = Inventory(self.items.dict, self.screen)

        # Movement speed management variable
        self.x_speed = 0
        self.y_speed = 0
        self.speed_factor = 4.0

        # Movement flags
        self.moving_up = False
        self.moving_down = False
        self.moving_left = False
        self.moving_right = False

    def update_pos(self):
        """
        Update mc gyver position using the movement flags and the speed
        factor of Mc Gyver (allowing him to move and sprint or not)
        This function create a new Rect object who will erase the current one
        if Mc Gyver can move in the desired direction.

        """
        # Check the movement flags
        if self.moving_up:
            self.y_speed = -1.0 * self.speed_factor
        if self.moving_down:
            self.y_speed = 1.0 * self.speed_factor
        if self.moving_left:
            self.x_speed = -1.0 * self.speed_factor
        if self.moving_right:
            self.x_speed = 1.0 * self.speed_factor

        # Update rect position
        newrect = self.rect.move(self.x_speed, self.y_speed)
        newrect_x = self.rect.move(self.x_speed, 0)
        newrect_y = self.rect.move(0, self.y_speed)
        if self.check_collisions(newrect):
            self.rect = newrect
        elif not self.check_collisions(newrect):
            rectbis = self.rect.move(self.x_speed / 2, self.y_speed / 2)
            if self.check_collisions(rectbis):
                self.rect = rectbis
            elif self.check_collisions(newrect_x):
                self.rect = newrect_x
            elif self.check_collisions(newrect_y):
                self.rect = newrect_y
        else:
            self.x_speed = 0
            self.y_speed = 0

    def update_inventory(self):
        """
        Manage the Mc Gyver inventory object when he encouter an object in the
        labyrinth (remove the object from the items dict in the inventory
        object).

        """
        collision_key = None
        for key, tile in self.items.dict.items():
            if self.rect.colliderect(tile.rect) and key != 'G':
                if key == 'ETHER1':
                    self.inventory.add_ether(key)
                    collision_key = key
                if key == 'ETHER2':
                    self.inventory.add_ether(key)
                    collision_key = key
                if key == 'ETHER3':
                    self.inventory.add_ether(key)
                    collision_key = key
                if key == 'ETHER4':
                    self.inventory.add_ether(key)
                    collision_key = key
                if key == 'NEEDLE':
                    self.inventory.add_needle(key)
                    collision_key = key
                if key == 'PLASTIC_TUBE':
                    self.inventory.add_plastic_tube(key)
                    collision_key = key
        if collision_key != None:
            del self.inventory.items_dict[collision_key]

    def update_state(self):
        """Allow Mc Gyver to escape the labyrinth"""
        if self.inventory.is_full():
            self.can_escape = True
        elif not self.inventory.is_full():
            self.can_escape = False

    def check_collisions(self, rect):
        """
        Check if there is a collision between the current rect and all
        the rects in the walls rect list.

        """
        if rect.collidelist(self.walls_list) == -1:
            return True
        else:
            return False

    def fight_guardian(self):
        """
        Return True if Mc Gyver met all the conditions needed to escape
        the labyrinth, False otherwise.

        """
        if self.rect.colliderect(self.inventory.items_dict['G'].rect):
            if self.can_escape is True:
                return True
            elif self.can_escape is False:
                return False

    def blitme(self):
        """Blit Mc Gyver on the screen"""
        self.screen.blit(self.image, self.rect)
