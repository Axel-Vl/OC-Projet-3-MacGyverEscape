from content.classes.tile import Tile


class Labyrinth:
    """
    Manage the main labyrinth structure(floor and walls),
    load the map from the labyrinth.txt file

    """

    def __init__(self, screen):
        self.screen = screen
        self.tiles_info_list = self._load_labyrinth_list()
        self.walls_tiles_list = []
        self.floor_tiles_list = []
        self._generate_tiles()

    def _load_labyrinth_list(self):
        """
        Load the labyrinth file and create a list with the XY coordinates
        and the characteristics of the tile

        """
        with open('content/labyrinth.txt', 'r') as labyrinth:
            lab_list = labyrinth.read().splitlines()
            final_lab_list = []
            y = 0
            for list in lab_list:
                x = 1
                y += 1
                for character in list:
                    characteristics = (x, y, character)
                    final_lab_list.append(characteristics)
                    x += 1

            return final_lab_list

    def _generate_tiles(self):
        """Generate the tiles from the tiles info list"""
        for tile_info in self.tiles_info_list:
            x = tile_info[0]
            y = tile_info[1]
            tile_type = tile_info[2]
            if tile_type == 'X':
                tile = Tile(x, y, tile_type)
                self.walls_tiles_list.append(tile)
            else:
                tile = Tile(x, y, tile_type)
                self.floor_tiles_list.append(tile)

    def blitme(self):
        """blit the walls and floor on the screen"""
        for tile in self.walls_tiles_list:
            self.screen.blit(tile.image, tile.rect)
        for tile in self.floor_tiles_list:
            self.screen.blit(tile.image, tile.rect)
