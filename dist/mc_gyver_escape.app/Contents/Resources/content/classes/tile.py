from pygame.sprite import Sprite
from random import randint
import content.general_functions as gf


class Tile(Sprite):
    """Load appropriate image for various objects and return a sprite"""

    def __init__(self, x, y, tile_type):
        Sprite.__init__(self)
        self.x_pos = x
        self.y_pos = y
        self.size = 32
        self.image, self.rect = self._choose_image_type(tile_type)
        self._move_rect()

    def _choose_image_type(self, tile_type):
        """Choose the appropriate image for the given tile_type"""
        z = randint(1, 2)
        if tile_type == ' ' and z == 1:
            return gf.load_img_rect('content/images/cracked_floor_tile.png')
        elif tile_type == ' ' and z == 2:
            return gf.load_img_rect('content/images/pristine_floor_tile.png')
        elif tile_type == 'X':
            return gf.load_img_rect('content/images/wall_tile.png')
        elif tile_type == 'G':
            return gf.load_img_rect('content/images/guardian.png')
        elif tile_type == 'ETHER' and z == 1:
            return gf.load_img_rect('content/images/ether.png')
        elif tile_type == 'ETHER' and z == 2:
            return gf.load_img_rect('content/images/ether_2.png')
        elif tile_type == 'NEEDLE':
            return gf.load_img_rect('content/images/needle.png')
        elif tile_type == 'PLASTIC_TUBE':
            return gf.load_img_rect('content/images/plastic_tube.png')

    def _move_rect(self):
        """Move all the tile to create a centered labyrinth on the screen"""
        self.rect = self.rect.move(self.x_pos * self.size + 68,
                                   self.y_pos * self.size + 68)
