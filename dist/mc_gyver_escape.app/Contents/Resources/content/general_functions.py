import pygame
from pygame.locals import *


def load_img_rect(image_name):
    """Load image and return image objet"""
    try:
        image = pygame.image.load(image_name)
        if image.get_alpha() is None:
            image = image.convert()
        else:
            image = image.convert_alpha()

    except pygame.error:
        print("Cannot load:", image_name)
        raise SystemExit

    return image, image.get_rect()


def load_img(image_name):
    """Load image and return image objet"""
    try:
        image = pygame.image.load(image_name)
        if image.get_alpha() is None:
            image = image.convert()
        else:
            image = image.convert_alpha()

    except pygame.error:
        print("Cannot load:", image_name)
        raise SystemExit

    return image
